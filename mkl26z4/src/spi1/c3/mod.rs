#[doc = r" Value read from the register"]
pub struct R {
    bits: u8,
}
#[doc = r" Value to write to the register"]
pub struct W {
    bits: u8,
}
impl super::C3 {
    #[doc = r" Modifies the contents of the register"]
    #[inline]
    pub fn modify<F>(&self, f: F)
    where
        for<'w> F: FnOnce(&R, &'w mut W) -> &'w mut W,
    {
        let bits = self.register.get();
        let r = R { bits: bits };
        let mut w = W { bits: bits };
        f(&r, &mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Reads the contents of the register"]
    #[inline]
    pub fn read(&self) -> R {
        R {
            bits: self.register.get(),
        }
    }
    #[doc = r" Writes to the register"]
    #[inline]
    pub fn write<F>(&self, f: F)
    where
        F: FnOnce(&mut W) -> &mut W,
    {
        let mut w = W::reset_value();
        f(&mut w);
        self.register.set(w.bits);
    }
    #[doc = r" Writes the reset value to the register"]
    #[inline]
    pub fn reset(&self) {
        self.write(|w| w)
    }
}
#[doc = "Possible values of the field `FIFOMODE`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum FIFOMODER {
    #[doc = "Buffer mode disabled"]
    _0,
    #[doc = "Data available in the receive data buffer"]
    _1,
}
impl FIFOMODER {
    #[doc = r" Returns `true` if the bit is clear (0)"]
    #[inline]
    pub fn bit_is_clear(&self) -> bool {
        !self.bit()
    }
    #[doc = r" Returns `true` if the bit is set (1)"]
    #[inline]
    pub fn bit_is_set(&self) -> bool {
        self.bit()
    }
    #[doc = r" Value of the field as raw bits"]
    #[inline]
    pub fn bit(&self) -> bool {
        match *self {
            FIFOMODER::_0 => false,
            FIFOMODER::_1 => true,
        }
    }
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _from(value: bool) -> FIFOMODER {
        match value {
            false => FIFOMODER::_0,
            true => FIFOMODER::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline]
    pub fn is_0(&self) -> bool {
        *self == FIFOMODER::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline]
    pub fn is_1(&self) -> bool {
        *self == FIFOMODER::_1
    }
}
#[doc = "Possible values of the field `RNFULLIEN`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum RNFULLIENR {
    #[doc = "No interrupt upon RNFULLF being set"]
    _0,
    #[doc = "Enable interrupts upon RNFULLF being set"]
    _1,
}
impl RNFULLIENR {
    #[doc = r" Returns `true` if the bit is clear (0)"]
    #[inline]
    pub fn bit_is_clear(&self) -> bool {
        !self.bit()
    }
    #[doc = r" Returns `true` if the bit is set (1)"]
    #[inline]
    pub fn bit_is_set(&self) -> bool {
        self.bit()
    }
    #[doc = r" Value of the field as raw bits"]
    #[inline]
    pub fn bit(&self) -> bool {
        match *self {
            RNFULLIENR::_0 => false,
            RNFULLIENR::_1 => true,
        }
    }
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _from(value: bool) -> RNFULLIENR {
        match value {
            false => RNFULLIENR::_0,
            true => RNFULLIENR::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline]
    pub fn is_0(&self) -> bool {
        *self == RNFULLIENR::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline]
    pub fn is_1(&self) -> bool {
        *self == RNFULLIENR::_1
    }
}
#[doc = "Possible values of the field `TNEARIEN`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TNEARIENR {
    #[doc = "No interrupt upon TNEAREF being set"]
    _0,
    #[doc = "Enable interrupts upon TNEAREF being set"]
    _1,
}
impl TNEARIENR {
    #[doc = r" Returns `true` if the bit is clear (0)"]
    #[inline]
    pub fn bit_is_clear(&self) -> bool {
        !self.bit()
    }
    #[doc = r" Returns `true` if the bit is set (1)"]
    #[inline]
    pub fn bit_is_set(&self) -> bool {
        self.bit()
    }
    #[doc = r" Value of the field as raw bits"]
    #[inline]
    pub fn bit(&self) -> bool {
        match *self {
            TNEARIENR::_0 => false,
            TNEARIENR::_1 => true,
        }
    }
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _from(value: bool) -> TNEARIENR {
        match value {
            false => TNEARIENR::_0,
            true => TNEARIENR::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline]
    pub fn is_0(&self) -> bool {
        *self == TNEARIENR::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline]
    pub fn is_1(&self) -> bool {
        *self == TNEARIENR::_1
    }
}
#[doc = "Possible values of the field `INTCLR`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum INTCLRR {
    #[doc = "These interrupts are cleared when the corresponding flags are cleared depending on the state of the FIFOs"]
    _0,
    #[doc = "These interrupts are cleared by writing the corresponding bits in the CI register"]
    _1,
}
impl INTCLRR {
    #[doc = r" Returns `true` if the bit is clear (0)"]
    #[inline]
    pub fn bit_is_clear(&self) -> bool {
        !self.bit()
    }
    #[doc = r" Returns `true` if the bit is set (1)"]
    #[inline]
    pub fn bit_is_set(&self) -> bool {
        self.bit()
    }
    #[doc = r" Value of the field as raw bits"]
    #[inline]
    pub fn bit(&self) -> bool {
        match *self {
            INTCLRR::_0 => false,
            INTCLRR::_1 => true,
        }
    }
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _from(value: bool) -> INTCLRR {
        match value {
            false => INTCLRR::_0,
            true => INTCLRR::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline]
    pub fn is_0(&self) -> bool {
        *self == INTCLRR::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline]
    pub fn is_1(&self) -> bool {
        *self == INTCLRR::_1
    }
}
#[doc = "Possible values of the field `RNFULLF_MARK`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum RNFULLF_MARKR {
    #[doc = "RNFULLF is set when the receive FIFO has 48 bits or more"]
    _0,
    #[doc = "RNFULLF is set when the receive FIFO has 32 bits or more"]
    _1,
}
impl RNFULLF_MARKR {
    #[doc = r" Returns `true` if the bit is clear (0)"]
    #[inline]
    pub fn bit_is_clear(&self) -> bool {
        !self.bit()
    }
    #[doc = r" Returns `true` if the bit is set (1)"]
    #[inline]
    pub fn bit_is_set(&self) -> bool {
        self.bit()
    }
    #[doc = r" Value of the field as raw bits"]
    #[inline]
    pub fn bit(&self) -> bool {
        match *self {
            RNFULLF_MARKR::_0 => false,
            RNFULLF_MARKR::_1 => true,
        }
    }
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _from(value: bool) -> RNFULLF_MARKR {
        match value {
            false => RNFULLF_MARKR::_0,
            true => RNFULLF_MARKR::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline]
    pub fn is_0(&self) -> bool {
        *self == RNFULLF_MARKR::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline]
    pub fn is_1(&self) -> bool {
        *self == RNFULLF_MARKR::_1
    }
}
#[doc = "Possible values of the field `TNEAREF_MARK`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TNEAREF_MARKR {
    #[doc = "TNEAREF is set when the transmit FIFO has 16 bits or less"]
    _0,
    #[doc = "TNEAREF is set when the transmit FIFO has 32 bits or less"]
    _1,
}
impl TNEAREF_MARKR {
    #[doc = r" Returns `true` if the bit is clear (0)"]
    #[inline]
    pub fn bit_is_clear(&self) -> bool {
        !self.bit()
    }
    #[doc = r" Returns `true` if the bit is set (1)"]
    #[inline]
    pub fn bit_is_set(&self) -> bool {
        self.bit()
    }
    #[doc = r" Value of the field as raw bits"]
    #[inline]
    pub fn bit(&self) -> bool {
        match *self {
            TNEAREF_MARKR::_0 => false,
            TNEAREF_MARKR::_1 => true,
        }
    }
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _from(value: bool) -> TNEAREF_MARKR {
        match value {
            false => TNEAREF_MARKR::_0,
            true => TNEAREF_MARKR::_1,
        }
    }
    #[doc = "Checks if the value of the field is `_0`"]
    #[inline]
    pub fn is_0(&self) -> bool {
        *self == TNEAREF_MARKR::_0
    }
    #[doc = "Checks if the value of the field is `_1`"]
    #[inline]
    pub fn is_1(&self) -> bool {
        *self == TNEAREF_MARKR::_1
    }
}
#[doc = "Values that can be written to the field `FIFOMODE`"]
pub enum FIFOMODEW {
    #[doc = "Buffer mode disabled"]
    _0,
    #[doc = "Data available in the receive data buffer"]
    _1,
}
impl FIFOMODEW {
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _bits(&self) -> bool {
        match *self {
            FIFOMODEW::_0 => false,
            FIFOMODEW::_1 => true,
        }
    }
}
#[doc = r" Proxy"]
pub struct _FIFOMODEW<'a> {
    w: &'a mut W,
}
impl<'a> _FIFOMODEW<'a> {
    #[doc = r" Writes `variant` to the field"]
    #[inline]
    pub fn variant(self, variant: FIFOMODEW) -> &'a mut W {
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "Buffer mode disabled"]
    #[inline]
    pub fn _0(self) -> &'a mut W {
        self.variant(FIFOMODEW::_0)
    }
    #[doc = "Data available in the receive data buffer"]
    #[inline]
    pub fn _1(self) -> &'a mut W {
        self.variant(FIFOMODEW::_1)
    }
    #[doc = r" Sets the field bit"]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r" Clears the field bit"]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r" Writes raw bits to the field"]
    #[inline]
    pub fn bit(self, value: bool) -> &'a mut W {
        const MASK: bool = true;
        const OFFSET: u8 = 0;
        self.w.bits &= !((MASK as u8) << OFFSET);
        self.w.bits |= ((value & MASK) as u8) << OFFSET;
        self.w
    }
}
#[doc = "Values that can be written to the field `RNFULLIEN`"]
pub enum RNFULLIENW {
    #[doc = "No interrupt upon RNFULLF being set"]
    _0,
    #[doc = "Enable interrupts upon RNFULLF being set"]
    _1,
}
impl RNFULLIENW {
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _bits(&self) -> bool {
        match *self {
            RNFULLIENW::_0 => false,
            RNFULLIENW::_1 => true,
        }
    }
}
#[doc = r" Proxy"]
pub struct _RNFULLIENW<'a> {
    w: &'a mut W,
}
impl<'a> _RNFULLIENW<'a> {
    #[doc = r" Writes `variant` to the field"]
    #[inline]
    pub fn variant(self, variant: RNFULLIENW) -> &'a mut W {
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "No interrupt upon RNFULLF being set"]
    #[inline]
    pub fn _0(self) -> &'a mut W {
        self.variant(RNFULLIENW::_0)
    }
    #[doc = "Enable interrupts upon RNFULLF being set"]
    #[inline]
    pub fn _1(self) -> &'a mut W {
        self.variant(RNFULLIENW::_1)
    }
    #[doc = r" Sets the field bit"]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r" Clears the field bit"]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r" Writes raw bits to the field"]
    #[inline]
    pub fn bit(self, value: bool) -> &'a mut W {
        const MASK: bool = true;
        const OFFSET: u8 = 1;
        self.w.bits &= !((MASK as u8) << OFFSET);
        self.w.bits |= ((value & MASK) as u8) << OFFSET;
        self.w
    }
}
#[doc = "Values that can be written to the field `TNEARIEN`"]
pub enum TNEARIENW {
    #[doc = "No interrupt upon TNEAREF being set"]
    _0,
    #[doc = "Enable interrupts upon TNEAREF being set"]
    _1,
}
impl TNEARIENW {
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _bits(&self) -> bool {
        match *self {
            TNEARIENW::_0 => false,
            TNEARIENW::_1 => true,
        }
    }
}
#[doc = r" Proxy"]
pub struct _TNEARIENW<'a> {
    w: &'a mut W,
}
impl<'a> _TNEARIENW<'a> {
    #[doc = r" Writes `variant` to the field"]
    #[inline]
    pub fn variant(self, variant: TNEARIENW) -> &'a mut W {
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "No interrupt upon TNEAREF being set"]
    #[inline]
    pub fn _0(self) -> &'a mut W {
        self.variant(TNEARIENW::_0)
    }
    #[doc = "Enable interrupts upon TNEAREF being set"]
    #[inline]
    pub fn _1(self) -> &'a mut W {
        self.variant(TNEARIENW::_1)
    }
    #[doc = r" Sets the field bit"]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r" Clears the field bit"]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r" Writes raw bits to the field"]
    #[inline]
    pub fn bit(self, value: bool) -> &'a mut W {
        const MASK: bool = true;
        const OFFSET: u8 = 2;
        self.w.bits &= !((MASK as u8) << OFFSET);
        self.w.bits |= ((value & MASK) as u8) << OFFSET;
        self.w
    }
}
#[doc = "Values that can be written to the field `INTCLR`"]
pub enum INTCLRW {
    #[doc = "These interrupts are cleared when the corresponding flags are cleared depending on the state of the FIFOs"]
    _0,
    #[doc = "These interrupts are cleared by writing the corresponding bits in the CI register"]
    _1,
}
impl INTCLRW {
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _bits(&self) -> bool {
        match *self {
            INTCLRW::_0 => false,
            INTCLRW::_1 => true,
        }
    }
}
#[doc = r" Proxy"]
pub struct _INTCLRW<'a> {
    w: &'a mut W,
}
impl<'a> _INTCLRW<'a> {
    #[doc = r" Writes `variant` to the field"]
    #[inline]
    pub fn variant(self, variant: INTCLRW) -> &'a mut W {
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "These interrupts are cleared when the corresponding flags are cleared depending on the state of the FIFOs"]
    #[inline]
    pub fn _0(self) -> &'a mut W {
        self.variant(INTCLRW::_0)
    }
    #[doc = "These interrupts are cleared by writing the corresponding bits in the CI register"]
    #[inline]
    pub fn _1(self) -> &'a mut W {
        self.variant(INTCLRW::_1)
    }
    #[doc = r" Sets the field bit"]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r" Clears the field bit"]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r" Writes raw bits to the field"]
    #[inline]
    pub fn bit(self, value: bool) -> &'a mut W {
        const MASK: bool = true;
        const OFFSET: u8 = 3;
        self.w.bits &= !((MASK as u8) << OFFSET);
        self.w.bits |= ((value & MASK) as u8) << OFFSET;
        self.w
    }
}
#[doc = "Values that can be written to the field `RNFULLF_MARK`"]
pub enum RNFULLF_MARKW {
    #[doc = "RNFULLF is set when the receive FIFO has 48 bits or more"]
    _0,
    #[doc = "RNFULLF is set when the receive FIFO has 32 bits or more"]
    _1,
}
impl RNFULLF_MARKW {
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _bits(&self) -> bool {
        match *self {
            RNFULLF_MARKW::_0 => false,
            RNFULLF_MARKW::_1 => true,
        }
    }
}
#[doc = r" Proxy"]
pub struct _RNFULLF_MARKW<'a> {
    w: &'a mut W,
}
impl<'a> _RNFULLF_MARKW<'a> {
    #[doc = r" Writes `variant` to the field"]
    #[inline]
    pub fn variant(self, variant: RNFULLF_MARKW) -> &'a mut W {
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "RNFULLF is set when the receive FIFO has 48 bits or more"]
    #[inline]
    pub fn _0(self) -> &'a mut W {
        self.variant(RNFULLF_MARKW::_0)
    }
    #[doc = "RNFULLF is set when the receive FIFO has 32 bits or more"]
    #[inline]
    pub fn _1(self) -> &'a mut W {
        self.variant(RNFULLF_MARKW::_1)
    }
    #[doc = r" Sets the field bit"]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r" Clears the field bit"]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r" Writes raw bits to the field"]
    #[inline]
    pub fn bit(self, value: bool) -> &'a mut W {
        const MASK: bool = true;
        const OFFSET: u8 = 4;
        self.w.bits &= !((MASK as u8) << OFFSET);
        self.w.bits |= ((value & MASK) as u8) << OFFSET;
        self.w
    }
}
#[doc = "Values that can be written to the field `TNEAREF_MARK`"]
pub enum TNEAREF_MARKW {
    #[doc = "TNEAREF is set when the transmit FIFO has 16 bits or less"]
    _0,
    #[doc = "TNEAREF is set when the transmit FIFO has 32 bits or less"]
    _1,
}
impl TNEAREF_MARKW {
    #[allow(missing_docs)]
    #[doc(hidden)]
    #[inline]
    pub fn _bits(&self) -> bool {
        match *self {
            TNEAREF_MARKW::_0 => false,
            TNEAREF_MARKW::_1 => true,
        }
    }
}
#[doc = r" Proxy"]
pub struct _TNEAREF_MARKW<'a> {
    w: &'a mut W,
}
impl<'a> _TNEAREF_MARKW<'a> {
    #[doc = r" Writes `variant` to the field"]
    #[inline]
    pub fn variant(self, variant: TNEAREF_MARKW) -> &'a mut W {
        {
            self.bit(variant._bits())
        }
    }
    #[doc = "TNEAREF is set when the transmit FIFO has 16 bits or less"]
    #[inline]
    pub fn _0(self) -> &'a mut W {
        self.variant(TNEAREF_MARKW::_0)
    }
    #[doc = "TNEAREF is set when the transmit FIFO has 32 bits or less"]
    #[inline]
    pub fn _1(self) -> &'a mut W {
        self.variant(TNEAREF_MARKW::_1)
    }
    #[doc = r" Sets the field bit"]
    pub fn set_bit(self) -> &'a mut W {
        self.bit(true)
    }
    #[doc = r" Clears the field bit"]
    pub fn clear_bit(self) -> &'a mut W {
        self.bit(false)
    }
    #[doc = r" Writes raw bits to the field"]
    #[inline]
    pub fn bit(self, value: bool) -> &'a mut W {
        const MASK: bool = true;
        const OFFSET: u8 = 5;
        self.w.bits &= !((MASK as u8) << OFFSET);
        self.w.bits |= ((value & MASK) as u8) << OFFSET;
        self.w
    }
}
impl R {
    #[doc = r" Value of the register as raw bits"]
    #[inline]
    pub fn bits(&self) -> u8 {
        self.bits
    }
    #[doc = "Bit 0 - FIFO mode enable"]
    #[inline]
    pub fn fifomode(&self) -> FIFOMODER {
        FIFOMODER::_from({
            const MASK: bool = true;
            const OFFSET: u8 = 0;
            ((self.bits >> OFFSET) & MASK as u8) != 0
        })
    }
    #[doc = "Bit 1 - Receive FIFO nearly full interrupt enable"]
    #[inline]
    pub fn rnfullien(&self) -> RNFULLIENR {
        RNFULLIENR::_from({
            const MASK: bool = true;
            const OFFSET: u8 = 1;
            ((self.bits >> OFFSET) & MASK as u8) != 0
        })
    }
    #[doc = "Bit 2 - Transmit FIFO nearly empty interrupt enable"]
    #[inline]
    pub fn tnearien(&self) -> TNEARIENR {
        TNEARIENR::_from({
            const MASK: bool = true;
            const OFFSET: u8 = 2;
            ((self.bits >> OFFSET) & MASK as u8) != 0
        })
    }
    #[doc = "Bit 3 - Interrupt clearing mechanism select"]
    #[inline]
    pub fn intclr(&self) -> INTCLRR {
        INTCLRR::_from({
            const MASK: bool = true;
            const OFFSET: u8 = 3;
            ((self.bits >> OFFSET) & MASK as u8) != 0
        })
    }
    #[doc = "Bit 4 - Receive FIFO nearly full watermark"]
    #[inline]
    pub fn rnfullf_mark(&self) -> RNFULLF_MARKR {
        RNFULLF_MARKR::_from({
            const MASK: bool = true;
            const OFFSET: u8 = 4;
            ((self.bits >> OFFSET) & MASK as u8) != 0
        })
    }
    #[doc = "Bit 5 - Transmit FIFO nearly empty watermark"]
    #[inline]
    pub fn tnearef_mark(&self) -> TNEAREF_MARKR {
        TNEAREF_MARKR::_from({
            const MASK: bool = true;
            const OFFSET: u8 = 5;
            ((self.bits >> OFFSET) & MASK as u8) != 0
        })
    }
}
impl W {
    #[doc = r" Reset value of the register"]
    #[inline]
    pub fn reset_value() -> W {
        W { bits: 0 }
    }
    #[doc = r" Writes raw bits to the register"]
    #[inline]
    pub unsafe fn bits(&mut self, bits: u8) -> &mut Self {
        self.bits = bits;
        self
    }
    #[doc = "Bit 0 - FIFO mode enable"]
    #[inline]
    pub fn fifomode(&mut self) -> _FIFOMODEW {
        _FIFOMODEW { w: self }
    }
    #[doc = "Bit 1 - Receive FIFO nearly full interrupt enable"]
    #[inline]
    pub fn rnfullien(&mut self) -> _RNFULLIENW {
        _RNFULLIENW { w: self }
    }
    #[doc = "Bit 2 - Transmit FIFO nearly empty interrupt enable"]
    #[inline]
    pub fn tnearien(&mut self) -> _TNEARIENW {
        _TNEARIENW { w: self }
    }
    #[doc = "Bit 3 - Interrupt clearing mechanism select"]
    #[inline]
    pub fn intclr(&mut self) -> _INTCLRW {
        _INTCLRW { w: self }
    }
    #[doc = "Bit 4 - Receive FIFO nearly full watermark"]
    #[inline]
    pub fn rnfullf_mark(&mut self) -> _RNFULLF_MARKW {
        _RNFULLF_MARKW { w: self }
    }
    #[doc = "Bit 5 - Transmit FIFO nearly empty watermark"]
    #[inline]
    pub fn tnearef_mark(&mut self) -> _TNEAREF_MARKW {
        _TNEAREF_MARKW { w: self }
    }
}
