#[doc = r" Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - SPI Status Register"]
    pub s: S,
    #[doc = "0x01 - SPI Baud Rate Register"]
    pub br: BR,
    #[doc = "0x02 - SPI Control Register 2"]
    pub c2: C2,
    #[doc = "0x03 - SPI Control Register 1"]
    pub c1: C1,
    #[doc = "0x04 - SPI Match Register low"]
    pub ml: ML,
    #[doc = "0x05 - SPI match register high"]
    pub mh: MH,
    #[doc = "0x06 - SPI Data Register low"]
    pub dl: DL,
    #[doc = "0x07 - SPI data register high"]
    pub dh: DH,
}
#[doc = "SPI Status Register"]
pub struct S {
    register: ::vcell::VolatileCell<u8>,
}
#[doc = "SPI Status Register"]
pub mod s;
#[doc = "SPI Baud Rate Register"]
pub struct BR {
    register: ::vcell::VolatileCell<u8>,
}
#[doc = "SPI Baud Rate Register"]
pub mod br;
#[doc = "SPI Control Register 2"]
pub struct C2 {
    register: ::vcell::VolatileCell<u8>,
}
#[doc = "SPI Control Register 2"]
pub mod c2;
#[doc = "SPI Control Register 1"]
pub struct C1 {
    register: ::vcell::VolatileCell<u8>,
}
#[doc = "SPI Control Register 1"]
pub mod c1;
#[doc = "SPI Match Register low"]
pub struct ML {
    register: ::vcell::VolatileCell<u8>,
}
#[doc = "SPI Match Register low"]
pub mod ml;
#[doc = "SPI match register high"]
pub struct MH {
    register: ::vcell::VolatileCell<u8>,
}
#[doc = "SPI match register high"]
pub mod mh;
#[doc = "SPI Data Register low"]
pub struct DL {
    register: ::vcell::VolatileCell<u8>,
}
#[doc = "SPI Data Register low"]
pub mod dl;
#[doc = "SPI data register high"]
pub struct DH {
    register: ::vcell::VolatileCell<u8>,
}
#[doc = "SPI data register high"]
pub mod dh;
